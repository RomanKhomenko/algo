package ru.khomenko.algo.heap;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Arrays;
import java.util.stream.Stream;


public class HeapTests {
    @ParameterizedTest
    @MethodSource("createTestData")
    void test(Integer[] input, Integer[] expected) {
        Heap<Integer> heap = new Heap<>(input);
        assertArrayEquals(expected, heap.getHeap());
    }

    private static Stream<Arguments> createTestData() {
        return Stream.of(
                Arguments.of(new Integer[]{1}, new Integer[]{1}),
                Arguments.of(new Integer[]{1, 2}, new Integer[]{2, 1}),
                Arguments.of(new Integer[]{1, 2, 3}, new Integer[]{3, 2, 1}),
                Arguments.of(new Integer[]{1, 2, 3, 4}, new Integer[]{4, 2, 3, 1}),
                Arguments.of(
                        new Integer[]{1, 2, 3, 4, 7, 8, 9, 10, 14, 16},
                        new Integer[]{16, 14, 9, 10, 7, 8, 3, 1, 4, 2}));
    }
}

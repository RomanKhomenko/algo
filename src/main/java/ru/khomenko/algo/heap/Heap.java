package ru.khomenko.algo.heap;

public class Heap<T extends  Comparable<T>> {
    public Heap(T[] array) {
        heap = array;
        buildHeap();
    }

    private static int parent(int i) {
        return (i + 1) / 2 - 1;
    }

    private static int left(int i) {
        return 2 * (i + 1) - 1;
    }

    private static int right(int i) {
        return 2 * (i + 1);
    }

    private void heapify(int i) {
        int left = left(i);
        int right = right(i);
        int largest = i;

        if (left < size && heap[left].compareTo(heap[i]) > 0) {
            largest = left;
        }

        if (right < size && heap[right].compareTo(heap[largest]) > 0) {
            largest = right;
        }

        if (largest != i) {
            T tmp = heap[i];
            heap[i] = heap[largest];
            heap[largest] = tmp;

            heapify(largest);
        }
    }

    private void buildHeap() {
        size = heap.length;

        for (int i = parent(size); i >= 0; i--) {
            heapify(i);
        }
    }

    public T[] getHeap() {
        return heap.clone();
    }

    private int size;
    private T[] heap;
}

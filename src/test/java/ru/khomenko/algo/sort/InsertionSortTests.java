package ru.khomenko.algo.sort;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Random;
import java.util.stream.IntStream;

class InsertionSortTests {
    @Test
    @DisplayName("Reversed array")
    void sortReversedArray() {
        Integer[] a = new Integer[]{9, 8, 7, 6, 5, 4, 3, 2, 1};
        Integer[] result = new Integer[]{1, 2, 3, 4, 5, 6, 7, 8, 9};
        InsertionSort.sort(a);
        assertArrayEquals(result, a);
    }

    @Test
    @DisplayName("Sorted Array")
    void sortSortedArray() {
        Integer[] a = new Integer[]{1, 2, 3, 4, 5, 6, 7, 8, 9};
        Integer[] result = new Integer[]{1, 2, 3, 4, 5, 6, 7, 8, 9};
        InsertionSort.sort(a);
        assertArrayEquals(result, a);
    }

    @Test
    @DisplayName("Sorted Array")
    void sortRandomArray() {
        Integer[] a = new Integer[]{5, 2, 4, 8, 1, 3, 7, 6, 9};
        Integer[] result = new Integer[]{1, 2, 3, 4, 5, 6, 7, 8, 9};
        InsertionSort.sort(a);
        assertArrayEquals(result, a);
    }

    @Test
    @DisplayName("Sort Huge Random Array")
    void sortHugeRandomArray() {
        int[] randArr = new Random().ints(10000, 0, 1000).toArray();
        Integer[] a = IntStream.of(randArr).boxed().toArray(Integer[]::new);
        Integer[] sorted = a.clone();
        Arrays.sort(sorted);
        InsertionSort.sort(a);
        assertArrayEquals(sorted, a);
    }
}

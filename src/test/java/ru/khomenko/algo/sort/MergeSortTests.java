package ru.khomenko.algo.sort;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class MergeSortTests {
    @Test
    @DisplayName("Two element array")
    void sortTwoElem() {
        Integer[] result = new Integer[]{1, 2};
        Integer[] a = new Integer[]{2, 1};
        MergeSort.sort(a);
        assertArrayEquals(result, a);
    }

    @Test
    @DisplayName("Three element array")
    void sortThreeElem() {
        Integer[] result = new Integer[]{1, 2, 3};
        Integer[] a = new Integer[]{2, 1, 3};
        MergeSort.sort(a);
        assertArrayEquals(result, a);
    }

    @Test
    @DisplayName("Sort Random Array")
    void sortRandomArray() {
        Integer[] a = new Integer[]{5, 2, 4, 8, 1, 3, 7, 6, 9};
        Integer[] result = new Integer[]{1, 2, 3, 4, 5, 6, 7, 8, 9};
        MergeSort.sort(a);
        assertArrayEquals(result, a);
    }
}

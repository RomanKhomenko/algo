package ru.khomenko.algo.sort;

import java.util.Arrays;

public class InsertionSort {
    public static <T extends Comparable<T>> void sort(T[] a) {
        if (a.length < 2) {
            return;
        }

        for (int i = 1; i < a.length; i++) {
            T key = a[i];
            int j = i - 1;

            while (j >= 0 && key.compareTo(a[j]) < 0) {
                a[j + 1] = a[j];
                j -= 1;
            }

            a[j + 1] = key;
        }
    }
}

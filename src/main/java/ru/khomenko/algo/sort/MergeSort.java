package ru.khomenko.algo.sort;

import java.lang.reflect.Array;

public class MergeSort {
    private static <T extends Comparable<T>> void merge(T[] a, int start, int middle, int end) {
        int leftSize = middle - start + 1;
        int rightSize = end - middle;

        Class<?> type = a.getClass().getComponentType();
        T[] left = (T[])Array.newInstance(type, leftSize + 1);
        T[] right = (T[])Array.newInstance(type, rightSize + 1);

        left[leftSize] = null;
        right[rightSize] = null;

        System.arraycopy(a, start, left, 0, leftSize);
        System.arraycopy(a, middle + 1, right, 0, rightSize);

        int i = 0;
        int j = 0;
        int k = 0;

        for (k = start; k < end; k++) {
            if (left[i] != null && right[j] != null && left[i].compareTo(right[j]) < 0) {
                a[k] = left[i];
                i += 1;
            }
            else {
                a[k] = right[j];
                j += 1;
            }
        }

        if (i <= leftSize - 1) {
            System.arraycopy(left, i, a, k, leftSize - i);
        }
        else if (j <= rightSize - 1){
            System.arraycopy(right, j, a, k, rightSize - j);
        }
    }

    private static <T extends Comparable<T>> void mergeSort(T[] a, int start, int end) {
        if (start < end) {
            int middle = (start + end) / 2;
            mergeSort(a, start, middle);
            mergeSort(a, middle + 1, end);
            merge(a, start, middle, end);
        }
    }

    public static <T extends Comparable<T>> void sort(T[] a) {
        mergeSort(a, 0, a.length - 1);
    }
}
